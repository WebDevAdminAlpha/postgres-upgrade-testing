# postgres-upgrade-testing

The aim of this project is to build the latest nightly image of GitLab, populate it with test data and then perform an 
upgrade of Postgres. Once upgraded the environment will be tested.